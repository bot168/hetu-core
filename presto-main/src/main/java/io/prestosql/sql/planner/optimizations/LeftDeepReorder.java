/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.prestosql.sql.planner.optimizations;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.VerifyException;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Ordering;
import io.airlift.log.Logger;
import io.airlift.units.Duration;
import io.prestosql.Session;
import io.prestosql.SystemSessionProperties;
import io.prestosql.cost.CachingCostProvider;
import io.prestosql.cost.CachingStatsProvider;
import io.prestosql.cost.CostCalculator;
import io.prestosql.cost.CostComparator;
import io.prestosql.cost.CostProvider;
import io.prestosql.cost.PlanCostEstimate;
import io.prestosql.cost.StatsCalculator;
import io.prestosql.cost.StatsProvider;
import io.prestosql.execution.warnings.WarningCollector;
import io.prestosql.matching.Pattern;
import io.prestosql.spi.PrestoException;
import io.prestosql.sql.planner.EqualityInference;
import io.prestosql.sql.planner.PlanNodeIdAllocator;
import io.prestosql.sql.planner.RuleStatsRecorder;
import io.prestosql.sql.planner.Symbol;
import io.prestosql.sql.planner.SymbolAllocator;
import io.prestosql.sql.planner.SymbolsExtractor;
import io.prestosql.sql.planner.TypeProvider;
import io.prestosql.sql.planner.iterative.GroupReference;
import io.prestosql.sql.planner.iterative.Lookup;
import io.prestosql.sql.planner.iterative.Memo;
import io.prestosql.sql.planner.iterative.Rule;
import io.prestosql.sql.planner.plan.JoinNode;
import io.prestosql.sql.planner.plan.PlanNode;
import io.prestosql.sql.planner.plan.TableScanNode;
import io.prestosql.sql.tree.ComparisonExpression;
import io.prestosql.sql.tree.Expression;
import io.prestosql.sql.tree.SymbolReference;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Predicates.in;
import static com.google.common.collect.ImmutableList.toImmutableList;
import static com.google.common.collect.ImmutableSet.toImmutableSet;
import static com.google.common.collect.Streams.stream;
import static io.prestosql.spi.StandardErrorCode.OPTIMIZER_TIMEOUT;
import static io.prestosql.sql.ExpressionUtils.and;
import static io.prestosql.sql.ExpressionUtils.extractConjuncts;
import static io.prestosql.sql.planner.DeterminismEvaluator.isDeterministic;
import static io.prestosql.sql.planner.EqualityInference.createEqualityInference;
import static io.prestosql.sql.planner.EqualityInference.nonInferrableConjuncts;
import static io.prestosql.sql.planner.plan.JoinNode.Type.FULL;
import static io.prestosql.sql.planner.plan.JoinNode.Type.INNER;
import static io.prestosql.sql.planner.plan.Patterns.join;
import static io.prestosql.sql.tree.BooleanLiteral.TRUE_LITERAL;
import static io.prestosql.sql.tree.ComparisonExpression.Operator.EQUAL;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.NANOSECONDS;

public class LeftDeepReorder
        implements PlanOptimizer
{
    private static final Logger log = Logger.get(LeftDeepReorder.class);

    private static final Pattern<JoinNode> PATTERN = join().matching(
            joinNode -> !joinNode.getDistributionType().isPresent()
                    && isDeterministic(joinNode.getFilter().orElse(TRUE_LITERAL)));

    private final RuleStatsRecorder stats;
    private final StatsCalculator statsCalculator;
    private final CostCalculator costCalculator;
    private final CostComparator costComparator;

    enum ChoosePattern {
        JP1, JP2, JP3
    }

    public LeftDeepReorder(RuleStatsRecorder stats, StatsCalculator statsCalculator, CostCalculator costCalculator, CostComparator costComparator)
    {
        this.stats = stats;
        this.statsCalculator = statsCalculator;
        this.costCalculator = costCalculator;
        this.costComparator = costComparator;
    }

    public PlanNode optimize(PlanNode plan, Session session, TypeProvider types, SymbolAllocator symbolAllocator, PlanNodeIdAllocator idAllocator, WarningCollector warningCollector)
    {
        // 1 found the left-deep join subtree(depends on the height limit)
        // 2 reorder
        Memo memo = new Memo(idAllocator, plan);
        Lookup lookup = Lookup.from(planNode -> Stream.of(memo.resolve(planNode)));
        Duration timeout = SystemSessionProperties.getOptimizerTimeout(session);
        Context context = new Context(memo, lookup, idAllocator, symbolAllocator, System.nanoTime(), timeout.toMillis(), session, warningCollector);
        RootLDJGroup rootLDJGroup = new RootLDJGroup(context);
        rootLDJGroup.searchLDJRoot(memo.getRootGroup());
        ChoosePattern pattern = rootLDJGroup.checkRule();
        log.debug("Group size: %s", rootLDJGroup.rootLeftDeepJoinGroup.size());
        for (PlanNode node : rootLDJGroup.rootLeftDeepJoinGroup) {
            int group = ((GroupReference) node).getGroupId();
            LMultiJoinNode lMultiJoinNode = LMultiJoinNode.toLMultiJoinNode((JoinNode) lookup.resolve(memo.getNode(group)), lookup, 20);
            lMultiJoinNode.solveConstraint(context);
            JoinOperator joinOperator = new JoinOperator(
                    costComparator,
                    memo,
                    lMultiJoinNode.getFilter(),
                    ruleContext(context),
                    lMultiJoinNode.specJoinCons,
                    pattern);
            LJoinResult result = joinOperator.generateResult(lMultiJoinNode.getSources(), lMultiJoinNode.getOutputSymbols(), lMultiJoinNode.leftOrders, lMultiJoinNode.joinTypes, lMultiJoinNode.fullJoins, lMultiJoinNode.sourceCount);
            if (result.equals(LJoinResult.UNKNOWN_COST_RESULT)) {
                log.debug("not a simple left deep join tree");
                continue;
            }
            context.memo.replace(group, result.getPlanNode().get(), LeftDeepReorder.class.toString());
        }
        return memo.extract();
    }

    static class JoinOperator
    {
        private final Session session;
        private final Memo memo;
        private final CostProvider costProvider;
        private final Ordering<LJoinResult> resultComparator;
        private final PlanNodeIdAllocator idAllocator;
        private final Expression allFilter;
        private final EqualityInference allFilterInference;
        private final Lookup lookup;
        private final Rule.Context context;
        private final Map<PlanNode, List<Expression>> specJoinCons;
        private final ChoosePattern pattern;

        @VisibleForTesting
        JoinOperator(CostComparator costComparator, Memo memo, Expression filter, Rule.Context context, Map<PlanNode, List<Expression>> specJoinCons, ChoosePattern pattern)
        {
            this.context = requireNonNull(context);
            this.memo = memo;
            this.session = requireNonNull(context.getSession(), "session is null");
            this.costProvider = requireNonNull(context.getCostProvider(), "costProvider is null");
            this.resultComparator = costComparator.forSession(session).onResultOf(result -> result.cost);
            this.idAllocator = requireNonNull(context.getIdAllocator(), "idAllocator is null");
            this.allFilter = requireNonNull(filter, "filter is null");
            this.allFilterInference = createEqualityInference(filter);
            this.lookup = requireNonNull(context.getLookup(), "lookup is null");
            this.specJoinCons = specJoinCons;
            this.pattern = pattern;
        }

        public LJoinResult generateResult(LinkedHashSet<PlanNode> sources, List<Symbol> outputSymbols, List<Pair<PlanNode, PlanNode>> leftOrders, Map<PlanNode, JoinNode.Type> joinTypes, Set<PlanNode> fullJoins, Map<PlanNode, LMultiJoinNode.Count> sourceCount)
        {
            List<LJoinResult> joinList = new ArrayList<>();
            List<PlanNode> doneList = new ArrayList<>();
            for (PlanNode node : sources) {
                PlanNode planNode = node;
                LJoinResult nodeResult = createJoinResult(planNode);
                if (nodeResult.equals(LJoinResult.UNKNOWN_COST_RESULT)) {
                    return LJoinResult.UNKNOWN_COST_RESULT;
                }
                joinList.add(nodeResult);
            }
            log.debug("tree size: %s", joinList.size());
            int constraintMost = 0;
            if (pattern == ChoosePattern.JP1) {
                constraintMost = getConstraintMost(joinList, joinTypes, sourceCount);
            }
            else if (pattern == ChoosePattern.JP2) {
                int size = joinList.size();
                if (size <= 12 && size >= 4) {
                    constraintMost = getConstraintMost(joinList, joinTypes);
                }
                else {
                    constraintMost = getConstraintMost(joinList, joinTypes, sourceCount);
                }
            }
            else {
                int size = joinList.size();
                if (size == 4) {
                    return LJoinResult.UNKNOWN_COST_RESULT;
                }
                else if (size > 4 && size < 7) {
                    constraintMost = getConstraintMost(joinList, joinTypes);
                }
                else {
                    constraintMost = getConstraintMost(joinList, joinTypes, sourceCount);
                }
            }

            LJoinResult joinResult = joinList.remove(constraintMost);
            doneList.add(lookup.resolve(joinResult.getPlanNode().get()));
            boolean isConstraintLeast = true;
            LJoinResult constraintResult = LJoinResult.UNKNOWN_COST_RESULT;
            boolean notConstraintLeast = true;
            LJoinResult constraintResultNot = LJoinResult.UNKNOWN_COST_RESULT;
            boolean isTheSecondOne = true;
            while (!joinList.isEmpty()) {
                isConstraintLeast = false;
                notConstraintLeast = false;
                List<LJoinResult> subJoinList = resultComparator.sortedCopy(joinList);
                for (LJoinResult nextJoinPart : subJoinList) {
                    if (joinTypes.containsKey(lookup.resolve(nextJoinPart.getPlanNode().get()))) {
                        if (!canPutIn(lookup.resolve(nextJoinPart.getPlanNode().get()), leftOrders, doneList)) {
                            continue;
                        }
                    }
                    if (!isConstraintLeast && isContraintJoin(doneList, nextJoinPart.getPlanNode().get(), joinTypes)) {
                        isConstraintLeast = true;
                        constraintResult = nextJoinPart;
                        break;
                    }
                    if (!notConstraintLeast) {
                        notConstraintLeast = true;
                        constraintResultNot = nextJoinPart;
                    }
                }
                if (isConstraintLeast) {
                    if (isTheSecondOne) {
                        isTheSecondOne = false;
                        int comResult = resultComparator.compare(joinResult, constraintResult);
                        if (comResult == -1 && !joinTypes.containsKey(lookup.resolve(constraintResult.getPlanNode().get()))) {
                            doneList.add(0, lookup.resolve(constraintResult.getPlanNode().get()));
                            joinList.remove(constraintResult);
                            continue;
                        }
                        doneList.add(lookup.resolve(constraintResult.getPlanNode().get()));
                        joinList.remove(constraintResult);
                        continue;
                    }
                    doneList.add(lookup.resolve(constraintResult.getPlanNode().get()));
                    joinList.remove(constraintResult);
                }
                else if (notConstraintLeast) {
                    if (isTheSecondOne) {
                        isTheSecondOne = false;
                        int comResult = resultComparator.compare(joinResult, constraintResultNot);
                        if (comResult == -1 && !joinTypes.containsKey(lookup.resolve(constraintResultNot.getPlanNode().get()))) {
                            doneList.add(0, lookup.resolve(constraintResultNot.getPlanNode().get()));
                            joinList.remove(constraintResultNot);
                            continue;
                        }
                        doneList.add(lookup.resolve(constraintResultNot.getPlanNode().get()));
                        joinList.remove(constraintResultNot);
                        continue;
                    }
                    doneList.add(lookup.resolve(constraintResultNot.getPlanNode().get()));
                    joinList.remove(constraintResultNot);
                }
                else {
                    return LJoinResult.UNKNOWN_COST_RESULT;
                }
            }
            return generatePartResult(doneList, outputSymbols, joinTypes, fullJoins);
        }

        private int getConstraintMost(List<LJoinResult> joinList, Map<PlanNode, JoinNode.Type> joinTypes)
        {
            int constraintMost = 0;
            int columns = 0;
            for (LJoinResult result : joinList) {
                if (result.getPlanNode().get().getOutputSymbols().size() > columns && !joinTypes.containsKey(lookup.resolve(result.getPlanNode().get()))) {
                    constraintMost = joinList.indexOf(result);
                    columns = result.getPlanNode().get().getOutputSymbols().size();
                }
            }
            return constraintMost;
        }

        private int getConstraintMost(List<LJoinResult> joinList, Map<PlanNode, JoinNode.Type> joinTypes, Map<PlanNode, LMultiJoinNode.Count> sourceCount)
        {
            int constraintMost = 0;
            int columns = 0;
            for (LJoinResult result : joinList) {
                int constraintNum = sourceCount.get(lookup.resolve(result.getPlanNode().get())).get();
                if (constraintNum > columns && !joinTypes.containsKey(lookup.resolve(result.getPlanNode().get()))) {
                    constraintMost = joinList.indexOf(result);
                    columns = constraintNum;
                }
            }
            return constraintMost;
        }

        private boolean isContraintJoin(List<PlanNode> donelist, PlanNode nextNode, Map<PlanNode, JoinNode.Type> joinTypes)
        {
            Set<Symbol> leftSymbols = donelist.stream()
                    .flatMap(node -> node.getOutputSymbols().stream())
                    .collect(toImmutableSet());
            Set<Symbol> rightSymbols = nextNode.getOutputSymbols().stream()
                    .collect(toImmutableSet());
            List<Expression> joinPredicates = getJoinPredicates(leftSymbols, rightSymbols, !joinTypes.containsKey(lookup.resolve(nextNode)), nextNode);
            List<JoinNode.EquiJoinClause> joinConditions = joinPredicates.stream()
                    .filter(JoinOperator::isJoinEqualityCondition)
                    .map(predicate -> toEquiJoinClause((ComparisonExpression) predicate, leftSymbols))
                    .collect(toImmutableList());
            if (joinConditions.isEmpty()) {
                return false;
            }
            return true;
        }

        private LJoinResult generatePartResult(List<PlanNode> donelist, List<Symbol> outputSymbols, Map<PlanNode, JoinNode.Type> joinTypes, Set<PlanNode> fullJoins)
        {
            JoinNode.Type type = INNER;
            if (donelist.size() == 2) {
                PlanNode left = donelist.get(0);
                PlanNode right = donelist.get(donelist.size() - 1);
                donelist.clear();
                Set<Symbol> leftSymbols = left.getOutputSymbols().stream()
                        .collect(toImmutableSet());
                Set<Symbol> rightSymbols = right.getOutputSymbols().stream()
                        .collect(toImmutableSet());
                List<Expression> joinPredicates = getJoinPredicates(leftSymbols, rightSymbols, !joinTypes.containsKey(lookup.resolve(right)), right);
                List<JoinNode.EquiJoinClause> joinConditions = joinPredicates.stream()
                        .filter(JoinOperator::isJoinEqualityCondition)
                        .map(predicate -> toEquiJoinClause((ComparisonExpression) predicate, leftSymbols))
                        .collect(toImmutableList());
                List<Expression> joinFilters = joinPredicates.stream()
                        .filter(predicate -> !isJoinEqualityCondition(predicate))
                        .collect(toImmutableList());

                List<Symbol> sortedOutputSymbols = Stream.concat(left.getOutputSymbols().stream(), right.getOutputSymbols().stream())
                        .filter(outputSymbols::contains)
                        .collect(toImmutableList());

                if (joinTypes.containsKey(lookup.resolve(right))) {
                    type = joinTypes.get(lookup.resolve(right));
                }
                if (fullJoins.contains(lookup.resolve(right))) {
                    type = FULL;
                }
                return createPartResult(new JoinNode(
                        idAllocator.getNextId(),
                        type,
                        left,
                        right,
                        joinConditions,
                        sortedOutputSymbols,
                        joinFilters.isEmpty() ? Optional.empty() : Optional.of(and(joinFilters)),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        ImmutableMap.of()));
            }
            else {
                PlanNode right = donelist.get(donelist.size() - 1);
                donelist.remove(right);
                Set<Symbol> leftSymbols = donelist.stream()
                        .flatMap(node -> node.getOutputSymbols().stream())
                        .collect(toImmutableSet());
                Set<Symbol> rightSymbols = right.getOutputSymbols().stream()
                        .collect(toImmutableSet());
                List<Expression> joinPredicates = getJoinPredicates(leftSymbols, rightSymbols, !joinTypes.containsKey(lookup.resolve(right)), right);
                List<JoinNode.EquiJoinClause> joinConditions = joinPredicates.stream()
                        .filter(JoinOperator::isJoinEqualityCondition)
                        .map(predicate -> toEquiJoinClause((ComparisonExpression) predicate, leftSymbols))
                        .collect(toImmutableList());
                List<Expression> joinFilters = joinPredicates.stream()
                        .filter(predicate -> !isJoinEqualityCondition(predicate))
                        .collect(toImmutableList());
                Set<Symbol> requiredJoinSymbols = ImmutableSet.<Symbol>builder()
                        .addAll(outputSymbols)
                        .addAll(SymbolsExtractor.extractUnique(joinPredicates))
                        .build();
                LJoinResult leftResult = generatePartResult(
                        donelist,
                        requiredJoinSymbols.stream()
                                .filter(leftSymbols::contains)
                                .collect(toImmutableList()),
                        joinTypes,
                        fullJoins);

                PlanNode left = leftResult.planNode.orElseThrow(() -> new VerifyException("Plan node is not present"));
                List<Symbol> sortedOutputSymbols = Stream.concat(left.getOutputSymbols().stream(), right.getOutputSymbols().stream())
                        .filter(outputSymbols::contains)
                        .collect(toImmutableList());

                if (joinTypes.containsKey(lookup.resolve(right))) {
                    type = joinTypes.get(lookup.resolve(right));
                }
                if (fullJoins.contains(lookup.resolve(right))) {
                    type = FULL;
                }
                return createPartResult(new JoinNode(
                        idAllocator.getNextId(),
                        type,
                        left,
                        right,
                        joinConditions,
                        sortedOutputSymbols,
                        joinFilters.isEmpty() ? Optional.empty() : Optional.of(and(joinFilters)),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        Optional.empty(),
                        ImmutableMap.of()));
            }
        }

        private LJoinResult createPartResult(PlanNode node)
        {
            return LJoinResult.createLJoinResult(Optional.of(node), PlanCostEstimate.zero());
        }

        private List<Expression> getJoinPredicates(Set<Symbol> leftSymbols, Set<Symbol> rightSymbols, boolean isChangeable, PlanNode node)
        {
            ImmutableList.Builder<Expression> joinPredicatesBuilder = ImmutableList.builder();

            // This takes all conjuncts that were part of allFilters that
            // could not be used for equality inference.
            // If they use both the left and right symbols, we add them to the list of joinPredicates
            stream(nonInferrableConjuncts(allFilter))
                    .map(conjunct -> allFilterInference.rewriteExpression(conjunct, symbol -> leftSymbols.contains(symbol) || rightSymbols.contains(symbol)))
                    .filter(Objects::nonNull)
                    // filter expressions that contain only left or right symbols
                    .filter(conjunct -> allFilterInference.rewriteExpression(conjunct, leftSymbols::contains) == null)
                    .filter(conjunct -> allFilterInference.rewriteExpression(conjunct, rightSymbols::contains) == null)
                    .forEach(joinPredicatesBuilder::add);

            // create equality inference on available symbols
            // TODO: make generateEqualitiesPartitionedBy take left and right scope
            List<Expression> joinEqualities = allFilterInference.generateEqualitiesPartitionedBy(symbol -> leftSymbols.contains(symbol) || rightSymbols.contains(symbol)).getScopeEqualities();
            EqualityInference joinInference = createEqualityInference(joinEqualities.toArray(new Expression[0]));
            joinPredicatesBuilder.addAll(joinInference.generateEqualitiesPartitionedBy(in(leftSymbols)).getScopeStraddlingEqualities());
            if (!isChangeable) {
                joinPredicatesBuilder.addAll(specJoinCons.get(lookup.resolve(node)));
            }
            return joinPredicatesBuilder.build();
        }

        private static boolean isJoinEqualityCondition(Expression expression)
        {
            return expression instanceof ComparisonExpression
                    && ((ComparisonExpression) expression).getOperator() == EQUAL
                    && ((ComparisonExpression) expression).getLeft() instanceof SymbolReference
                    && ((ComparisonExpression) expression).getRight() instanceof SymbolReference;
        }

        private static JoinNode.EquiJoinClause toEquiJoinClause(ComparisonExpression equality, Set<Symbol> leftSymbols)
        {
            Symbol leftSymbol = Symbol.from(equality.getLeft());
            Symbol rightSymbol = Symbol.from(equality.getRight());
            JoinNode.EquiJoinClause equiJoinClause = new JoinNode.EquiJoinClause(leftSymbol, rightSymbol);
            return leftSymbols.contains(leftSymbol) ? equiJoinClause : equiJoinClause.flip();
        }

        private boolean canPutIn(PlanNode node, List<Pair<PlanNode, PlanNode>> leftOrders, List<PlanNode> doneList)
        {
            for (Pair<PlanNode, PlanNode> pair : leftOrders) {
                if (lookup.resolve(pair.getValue()).equals(node) && !doneList.contains(lookup.resolve(pair.getKey()))) {
                    return false;
                }
            }
            return true;
        }

        private LJoinResult createJoinResult(PlanNode planNode)
        {
            LJoinResult result = LJoinResult.createLJoinResult(Optional.of(planNode), costProvider.getCost(planNode));
            if (result.equals(LJoinResult.UNKNOWN_COST_RESULT)) {
                return LJoinResult.createLJoinResult(Optional.of(planNode), getSourceResult(planNode));
            }
            return result;
        }

        private PlanCostEstimate getSourceResult(PlanNode planNode)
        {
            if (lookup.resolve(planNode) instanceof TableScanNode) {
                return costProvider.getCost(planNode);
            }
            if (lookup.resolve(planNode).getSources().size() > 1) {
                return LJoinResult.UNKNOWN_COST_RESULT.getCost();
            }
            return getSourceResult(lookup.resolve(planNode).getSources().get(0));
        }
    }

    private Rule.Context ruleContext(Context context)
    {
        StatsProvider statsProvider = new CachingStatsProvider(statsCalculator, Optional.of(context.memo), context.lookup, context.session, context.symbolAllocator.getTypes());
        CostProvider costProvider = new CachingCostProvider(costCalculator, statsProvider, Optional.of(context.memo), context.session, context.symbolAllocator.getTypes());

        return new Rule.Context()
        {
            @Override
            public Lookup getLookup()
            {
                return context.lookup;
            }

            @Override
            public PlanNodeIdAllocator getIdAllocator()
            {
                return context.idAllocator;
            }

            @Override
            public SymbolAllocator getSymbolAllocator()
            {
                return context.symbolAllocator;
            }

            @Override
            public Session getSession()
            {
                return context.session;
            }

            @Override
            public StatsProvider getStatsProvider()
            {
                return statsProvider;
            }

            @Override
            public CostProvider getCostProvider()
            {
                return costProvider;
            }

            @Override
            public void checkTimeoutNotExhausted()
            {
                context.checkTimeoutNotExhausted();
            }

            @Override
            public WarningCollector getWarningCollector()
            {
                return context.warningCollector;
            }
        };
    }

    // found the first join in the tree
    // -1 for not found, -2 for overlimit
    private int foundJoin(int group, Context context, int limit)
    {
        if (limit == 0) {
            return -2;
        }
        PlanNode node = context.memo.getNode(group);
        PlanNode resolved = context.lookup.resolve(node);
        if (resolved instanceof JoinNode) {
            return group;
        }
        for (PlanNode child : node.getSources()) {
            int childgroup = foundJoin(((GroupReference) child).getGroupId(), context, limit - 1);
            if (childgroup != -1 && childgroup != -2) {
                return childgroup;
            }
        }
        return -1;
    }

    private boolean isLeftDeep(JoinNode joinNode, Context context)
    {
        if (joinNode.getRight() instanceof JoinNode) {
            return false;
        }
        else if (joinNode.getLeft() instanceof JoinNode) {
            return isLeftDeep((JoinNode) joinNode.getLeft(), context);
        }
        return true;
    }

    static class RootLDJGroup
    {
        private List<PlanNode> rootLeftDeepJoinGroup = new ArrayList<>();
        private Context context;

        public RootLDJGroup(Context context)
        {
            this.context = context;
        }

        public void searchLDJRoot(int rootGroup)
        {
            //actually the root should be outputNode, but in case it is not, we will check
            PlanNode rootNode = context.memo.getNode(rootGroup);
            PlanNode resolved = context.lookup.resolve(rootNode);
            if (resolved instanceof JoinNode) {
                if (checkLeftDeepTree(rootGroup) == 1) {
                    rootLeftDeepJoinGroup.add(rootNode);
                }
            }
            else {
                checkLeftDeepTree(rootGroup);
            }
        }

        public ChoosePattern checkRule()
        {
            int groupSize = rootLeftDeepJoinGroup.size();
            if (groupSize > 7) {
                return ChoosePattern.JP1;
            }
            if (groupSize == 1) {
                return ChoosePattern.JP2;
            }
            return ChoosePattern.JP3;
        }

        //-1 stands for no join, 0 stands for having join but not left deep tree, 1 stands for left deep tree
        public int checkLeftDeepTree(int group)
        {
            PlanNode node = context.memo.getNode(group);
            PlanNode resolved = context.lookup.resolve(node);
            if (resolved instanceof JoinNode) {
                int leftNodeID = ((GroupReference) ((JoinNode) resolved).getLeft()).getGroupId();
                int rightNodeID = ((GroupReference) ((JoinNode) resolved).getRight()).getGroupId();
                int leftReturn = checkLeftDeepTree(leftNodeID);
                int rightReturn = checkLeftDeepTree(rightNodeID);
                // if left sub tree is left-deep join tree while right get no join
                if (leftReturn == 1 && rightReturn == -1) {
                    return 1;
                }
                else if (leftReturn == 1 && rightReturn != -1) {
                    rootLeftDeepJoinGroup.add(((JoinNode) resolved).getLeft());
                    return 0;
                }
                else if (leftReturn == -1 && rightReturn == -1) {
                    // it is a left deep tree root
                    return 1;
                }
                else {
                    return 0;
                }
            }
            List<PlanNode> children = node.getSources();
            if (children.size() > 1) {
                boolean hasJoin = false;
                for (PlanNode child : children) {
                    int childReturn = checkLeftDeepTree(((GroupReference) child).getGroupId());
                    if (childReturn == 1) {
                        hasJoin = true;
                        rootLeftDeepJoinGroup.add(child);
                    }
                    else if (childReturn == 0) {
                        hasJoin = true;
                    }
                }
                if (hasJoin) {
                    return 0;
                }
                return -1;
            }
            else if (children.size() == 1) {
                int childReturn = checkLeftDeepTree(((GroupReference) children.get(0)).getGroupId());
                if (childReturn == 1) {
                    rootLeftDeepJoinGroup.add(children.get(0));
                    return 0;
                }
                else {
                    return childReturn;
                }
            }
            else {
                return -1;
            }
        }
    }

    private boolean exploreGroup(int group, Context context)
    {
        exploreNode(group, context);
        return true;
    }

    private boolean exploreNode(int group, Context context)
    {
        PlanNode node = context.memo.getNode(group);
        log.debug("%s", context.lookup.resolve(node).getClass());
        if (context.lookup.resolve(node) instanceof JoinNode) {
            log.debug("%s", ((JoinNode) context.lookup.resolve(node)).getType());
            log.debug("%s", ((JoinNode) context.lookup.resolve(node)).getCriteria());
        }
        log.debug("%s", node.getOutputSymbols());
        for (PlanNode child : node.getSources()) {
            exploreNode(((GroupReference) child).getGroupId(), context);
        }
        return true;
    }

    private static class Context
    {
        private final Memo memo;
        private final Lookup lookup;
        private final PlanNodeIdAllocator idAllocator;
        private final SymbolAllocator symbolAllocator;
        private final long startTimeInNanos;
        private final long timeoutInMilliseconds;
        private final Session session;
        private final WarningCollector warningCollector;

        public Context(
                Memo memo,
                Lookup lookup,
                PlanNodeIdAllocator idAllocator,
                SymbolAllocator symbolAllocator,
                long startTimeInNanos,
                long timeoutInMilliseconds,
                Session session,
                WarningCollector warningCollector)
        {
            checkArgument(timeoutInMilliseconds >= 0, "Timeout has to be a non-negative number [milliseconds]");

            this.memo = memo;
            this.lookup = lookup;
            this.idAllocator = idAllocator;
            this.symbolAllocator = symbolAllocator;
            this.startTimeInNanos = startTimeInNanos;
            this.timeoutInMilliseconds = timeoutInMilliseconds;
            this.session = session;
            this.warningCollector = warningCollector;
        }

        public void checkTimeoutNotExhausted()
        {
            if ((NANOSECONDS.toMillis(System.nanoTime() - startTimeInNanos)) >= timeoutInMilliseconds) {
                throw new PrestoException(OPTIMIZER_TIMEOUT, format("The optimizer exhausted the time limit of %d ms", timeoutInMilliseconds));
            }
        }
    }
    /**
     * This class represents a set of joins with some order constraints
     */

    @VisibleForTesting
    static class LMultiJoinNode
    {
        // Use a linked hash set to ensure optimizer is deterministic
        private final LinkedHashSet<PlanNode> sources;
        private final Map<PlanNode, Count> sourceCount;
        private final Expression filter;
        private final List<Symbol> outputSymbols;
        private final Map<PlanNode, PlanNode> constraint;
        private List<Pair<PlanNode, PlanNode>> leftOrders;
        private final Map<PlanNode, JoinNode.Type> joinTypes;
        private final Set<PlanNode> fullJoins;
        private final Map<PlanNode, List<Expression>> specJoinCons;

        public LMultiJoinNode(LinkedHashSet<PlanNode> sources, Expression filter, List<Symbol> outputSymbols, Map<PlanNode, PlanNode> constraint, Map<PlanNode, JoinNode.Type> joinTypes, Set<PlanNode> fullJoins, Map<PlanNode, List<Expression>> specJoinCons, Map<PlanNode, Count> sourceCount)
        {
            requireNonNull(sources, "sources is null");
            checkArgument(sources.size() > 1, "sources size is <= 1");
            requireNonNull(filter, "filter is null");
            requireNonNull(outputSymbols, "outputSymbols is null");

            this.sources = sources;
            this.filter = filter;
            this.outputSymbols = ImmutableList.copyOf(outputSymbols);
            this.constraint = constraint;
            this.joinTypes = joinTypes;
            this.fullJoins = fullJoins;
            this.specJoinCons = specJoinCons;
            this.sourceCount = sourceCount;

            List<Symbol> inputSymbols = sources.stream().flatMap(source -> source.getOutputSymbols().stream()).collect(toImmutableList());
            checkArgument(inputSymbols.containsAll(outputSymbols), "inputs do not contain all output symbols");
        }

        public Expression getFilter()
        {
            return filter;
        }

        public LinkedHashSet<PlanNode> getSources()
        {
            return sources;
        }

        public List<Symbol> getOutputSymbols()
        {
            return outputSymbols;
        }

        public static LMultiJoinNode.Builder builder()
        {
            return new LMultiJoinNode.Builder();
        }

        @Override
        public int hashCode()
        {
            return Objects.hash(sources, ImmutableSet.copyOf(extractConjuncts(filter)), outputSymbols);
        }

        @Override
        public boolean equals(Object obj)
        {
            if (!(obj instanceof LMultiJoinNode)) {
                return false;
            }

            LMultiJoinNode other = (LMultiJoinNode) obj;
            return this.sources.equals(other.sources)
                    && ImmutableSet.copyOf(extractConjuncts(this.filter)).equals(ImmutableSet.copyOf(extractConjuncts(other.filter)))
                    && this.outputSymbols.equals(other.outputSymbols);
        }

        public void solveConstraint(Context context)
        {
            this.leftOrders = new ArrayList<>();
            List<PlanNode> checkedList = new ArrayList<>();
            for (Map.Entry<PlanNode, PlanNode> entry : constraint.entrySet()) {
                PlanNode left = context.lookup.resolve(entry.getKey());
                PlanNode right = context.lookup.resolve(entry.getValue());
                checkedList.clear();
                if (left instanceof JoinNode) {
                    JoinNode joinNode = (JoinNode) left;
                    List<JoinNode.EquiJoinClause> clauses = joinNode.getCriteria();
                    for (JoinNode.EquiJoinClause clause : clauses) {
                        for (PlanNode node : this.sources) {
                            if (node.getOutputSymbols().contains(clause.getLeft())) {
                                if (!checkedList.contains(node)) {
                                    leftOrders.add(new Pair<>(context.lookup.resolve(node), right));
                                    checkedList.add(node);
                                }
                                break;
                            }
                        }
                    }
                }
                else {
                    log.debug("insert wrong things");
                    continue;
                }
            }
        }

        static LMultiJoinNode toLMultiJoinNode(JoinNode joinNode, Lookup lookup, int joinLimit)
        {
            // the number of sources is the number of joins + 1
            return new LMultiJoinNode.JoinNodeFlattener(joinNode, lookup, joinLimit + 1).toLMultiJoinNode();
        }

        private static class Count
        {
            private int count;

            public Count()
            {
                count = 0;
            }

            public void add()
            {
                count++;
            }

            public int get()
            {
                return count;
            }
        }

        private static class JoinNodeFlattener
        {
            private final LinkedHashSet<PlanNode> sources = new LinkedHashSet<>();
            private final Map<PlanNode, Count> sourceCount;
            private final List<Expression> filters = new ArrayList<>();
            private final List<Symbol> outputSymbols;
            private final Lookup lookup;
            private final Map<PlanNode, PlanNode> constraint;
            private final Map<PlanNode, JoinNode.Type> joinTypes;
            private final Set<PlanNode> fullJoins;
            private final Map<PlanNode, List<Expression>> specJoinCons;
            private final List<JoinNode.EquiJoinClause> joinClauses;

            JoinNodeFlattener(JoinNode node, Lookup lookup, int sourceLimit)
            {
                requireNonNull(node, "node is null");
                this.outputSymbols = node.getOutputSymbols();
                this.lookup = requireNonNull(lookup, "lookup is null");
                this.constraint = new HashMap<>();
                this.joinTypes = new HashMap<>();
                this.fullJoins = new HashSet<>();
                this.specJoinCons = new HashMap<>();
                this.sourceCount = new HashMap<>();
                this.joinClauses = new ArrayList<>();
                flattenNode(node, sourceLimit);
                solveCount();
            }

            private void flattenNode(PlanNode node, int limit)
            {
                PlanNode resolved = lookup.resolve(node);
                // (limit - 2) because you need to account for adding left and right side
                if (!(resolved instanceof JoinNode) || (sources.size() > (limit - 2))) {
                    sources.add(node);
                    Count count = new Count();
                    sourceCount.put(lookup.resolve(node), count);
                    return;
                }

                // check constraint
                JoinNode joinNode = (JoinNode) resolved;
                joinClauses.addAll(joinNode.getCriteria());
                if (joinNode.getType() != INNER && joinNode.getType() != FULL) {
                    constraint.put(joinNode, joinNode.getRight());
                    joinTypes.put(lookup.resolve(lookup.resolve(joinNode.getRight())), joinNode.getType());
                    List<Expression> specJoinCon = new ArrayList<>();
                    joinNode.getCriteria().stream()
                            .map(JoinNode.EquiJoinClause::toExpression)
                            .forEach(specJoinCon::add);
                    joinNode.getFilter().ifPresent(specJoinCon::add);
                    specJoinCons.put(lookup.resolve(lookup.resolve(joinNode.getRight())), specJoinCon);
                }
                if (joinNode.getType() == FULL) {
                    fullJoins.add(lookup.resolve(joinNode.getRight()));
                }

                // we set the left limit to limit - 1 to account for the node on the right
                flattenNode(joinNode.getLeft(), limit - 1);
                flattenNode(joinNode.getRight(), limit);
                if (joinNode.getType() == INNER || joinNode.getType() == FULL) {
                    joinNode.getCriteria().stream()
                            .map(JoinNode.EquiJoinClause::toExpression)
                            .forEach(filters::add);
                    joinNode.getFilter().ifPresent(filters::add);
                }
            }

            private void solveCount()
            {
                for (JoinNode.EquiJoinClause clause : joinClauses) {
                    for (PlanNode node : sourceCount.keySet()) {
                        if (node.getOutputSymbols().contains(clause.getLeft())) {
                            sourceCount.get(node).add();
                            break;
                        }
                    }
                    for (PlanNode node : sourceCount.keySet()) {
                        if (node.getOutputSymbols().contains(clause.getRight())) {
                            sourceCount.get(node).add();
                            break;
                        }
                    }
                }
            }

            LMultiJoinNode toLMultiJoinNode()
            {
                return new LMultiJoinNode(sources, and(filters), outputSymbols, constraint, joinTypes, fullJoins, specJoinCons, sourceCount);
            }
        }

        static class Builder
        {
            private List<PlanNode> sources;
            private Expression filter;
            private List<Symbol> outputSymbols;
            private Map<PlanNode, PlanNode> constraint;
            private Map<PlanNode, JoinNode.Type> joinTypes;
            private Set<PlanNode> fullJoins;
            private Map<PlanNode, List<Expression>> specJoinCons;
            private Map<PlanNode, Count> joinClauses;

            public LMultiJoinNode.Builder setSources(PlanNode... sources)
            {
                this.sources = ImmutableList.copyOf(sources);
                return this;
            }

            public LMultiJoinNode.Builder setFilter(Expression filter)
            {
                this.filter = filter;
                return this;
            }

            public LMultiJoinNode.Builder setOutputSymbols(Symbol... outputSymbols)
            {
                this.outputSymbols = ImmutableList.copyOf(outputSymbols);
                return this;
            }

            public LMultiJoinNode.Builder setConstraint(Map<PlanNode, PlanNode> constraint)
            {
                this.constraint = constraint;
                return this;
            }

            public LMultiJoinNode.Builder setJoinTypes(Map<PlanNode, JoinNode.Type> joinTypes)
            {
                this.joinTypes = joinTypes;
                return this;
            }

            public LMultiJoinNode.Builder setFullJoins(Set<PlanNode> fullJoins)
            {
                this.fullJoins = fullJoins;
                return this;
            }

            public LMultiJoinNode build()
            {
                return new LMultiJoinNode(new LinkedHashSet<>(sources), filter, outputSymbols, constraint, joinTypes, fullJoins, specJoinCons, joinClauses);
            }
        }
    }

    @VisibleForTesting
    static class LJoinResult
    {
        public static final LJoinResult UNKNOWN_COST_RESULT = new LJoinResult(Optional.empty(), PlanCostEstimate.unknown());
        public static final LJoinResult INFINITE_COST_RESULT = new LJoinResult(Optional.empty(), PlanCostEstimate.infinite());

        private final Optional<PlanNode> planNode;
        private final PlanCostEstimate cost;

        private LJoinResult(Optional<PlanNode> planNode, PlanCostEstimate cost)
        {
            this.planNode = requireNonNull(planNode, "planNode is null");
            this.cost = requireNonNull(cost, "cost is null");
            checkArgument((cost.hasUnknownComponents() || cost.equals(PlanCostEstimate.infinite())) && !planNode.isPresent()
                            || (!cost.hasUnknownComponents() || !cost.equals(PlanCostEstimate.infinite())) && planNode.isPresent(),
                    "planNode should be present if and only if cost is known");
        }

        public Optional<PlanNode> getPlanNode()
        {
            return planNode;
        }

        public PlanCostEstimate getCost()
        {
            return cost;
        }

        static LJoinResult createLJoinResult(Optional<PlanNode> planNode, PlanCostEstimate cost)
        {
            if (cost.hasUnknownComponents()) {
                return UNKNOWN_COST_RESULT;
            }
            if (cost.equals(PlanCostEstimate.infinite())) {
                return INFINITE_COST_RESULT;
            }
            return new LJoinResult(planNode, cost);
        }
    }
}
