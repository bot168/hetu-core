/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.prestosql.cli;

import com.google.common.collect.ImmutableList;
import io.prestosql.sql.parser.ParsingOptions;
import io.prestosql.sql.parser.SqlParser;
import io.prestosql.sql.tree.CreateTableAsSelect;
import io.prestosql.sql.tree.DefaultTraversalVisitor;
import io.prestosql.sql.tree.DropTable;
import io.prestosql.sql.tree.QualifiedName;
import io.prestosql.sql.tree.Query;
import io.prestosql.sql.tree.Table;
import io.prestosql.sql.tree.WithQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ReUseWithClause
{
    private final String initSql;
    private final List<Query> inputQueries;
    private final List<CreateTableAsSelect> immidiateTableList;
    private final List<Query> outputQueries;
    private final List<DropTable> dropTableList;
    private final TreePrintToSql printer;
    private final SqlParser parser;
    private final Map<String, CountInt> withTableNameList;
    private boolean isIn;

    public ReUseWithClause(String initSql)
    {
        this.initSql = initSql;
        this.parser = new SqlParser();
        this.inputQueries = new ArrayList<>();
        this.immidiateTableList = new ArrayList<>();
        this.outputQueries = new ArrayList<>();
        this.dropTableList = new ArrayList<>();
        this.printer = new TreePrintToSql();
        this.withTableNameList = new HashMap<>();
    }

    private void init()
    {
        inputQueries.clear();
        String[] initSqls;
        String delimeter = ";";  // 指定分割字符
        initSqls = this.initSql.split(delimeter); // 分割字符串
        for (int i = 0; i < initSqls.length; i++) {
            Query query = (Query) parser.createStatement(initSqls[i], new ParsingOptions(ParsingOptions.DecimalLiteralTreatment.AS_DOUBLE));
            inputQueries.add(query);
        }
    }

    private void getCreateTableName()
    {
        init();
        DefaultTraversalVisitor<Void, Void> counter = new DefaultTraversalVisitor<Void, Void>()
        {
            @Override
            protected Void visitTable(Table node, Void context)
            {
                String tableName = node.getName().toString();
//                System.out.print("table: " + tableName + "\n");
                if (withTableNameList.containsKey(tableName)) {
                    withTableNameList.get(tableName).add();
                    if (isIn) {
                        withTableNameList.get(tableName).add();
                    }
                }
                return null;
            }

            @Override
            protected Void visitWithQuery(WithQuery node, Void context)
            {
                isIn = false;
                String tableName = node.getName().getValue();
//                System.out.print("withquery name: " + tableName + "\n");
                if (!withTableNameList.containsKey(tableName)) {
                    isIn = true;
                    withTableNameList.put(tableName, new CountInt());
                }
                this.process(node.getQuery());
                isIn = false;
                return null;
            }
        };

        for (Query query : this.inputQueries) {
            counter.process(query);
        }

        DefaultTraversalVisitor<Void, Void> replacer = new DefaultTraversalVisitor<Void, Void>()
        {
            @Override
            protected Void visitTable(Table node, Void context)
            {
                String tableName = node.getName().toString();
//                System.out.print("table: " + tableName + "\n");
                if (withTableNameList.containsKey(tableName)) {
                    if (withTableNameList.get(tableName).get() > 1) {
                        node.setName(QualifiedName.of("memory", "default", tableName));
                    }
                }
//                System.out.print("table: " + node.getName().toString() + "\n");
                return null;
            }
        };

        for (Query query : this.inputQueries) {
            replacer.process(query);
        }

        for (String key : this.withTableNameList.keySet()) {
//            System.out.print(key + ": " + this.withTableNameList.get(key).get() + "\n");
        }
    }

    public void createCreateTableAsSelectSql()
    {
        getCreateTableName();
        List<WithQuery> withList = new ArrayList<>();
        for (Query query : this.inputQueries) {
            if (query.getWith().isPresent()) {
                boolean useLessWith = false;
                List<WithQuery> waitDelete = new ArrayList<>();
                for (int i = 0; i < query.getWith().get().getQueries().size(); i++) {
                    WithQuery withquery = query.getWith().get().getQueries().get(i);
//                    System.out.print("with query: " + withquery + "\n");
//                    System.out.print("withList: " + withList + "\n");
                    for (WithQuery item : withList) {
                        if (withquery.equals(item)) {
                            useLessWith = true;
                            break;
                        }
                    }
//                    System.out.print("useLessWith: " + useLessWith + "\n");
//                    System.out.print(withquery.getName().getValue() + "\n");
                    String keyTableName = withquery.getName().getValue();
                    if (!useLessWith && withTableNameList.containsKey(keyTableName)) {
                        if (withTableNameList.get(keyTableName).get() > 1) {
                            withList.add(withquery);
//                             System.out.print("add with query\n");
                            waitDelete.add(withquery);
//                              System.out.print("removeQuery" + withquery.getName().getValue() + "\n");
                        }
                    }
                    else if (withTableNameList.containsKey(withquery.getName().getValue())) {
                        if (withTableNameList.get(keyTableName).get() > 1) {
                            waitDelete.add(withquery);
                        }
                    }
                }
                for (WithQuery item : waitDelete) {
                    query.getWith().get().removeQuery(item);
                }
//                System.out.print(query.getWith().get().getQueries().size() + "\n");
            }
            this.outputQueries.add(query);
        }
//        System.out.print("find all with query\n");
        for (WithQuery withQuery : withList) {
            QualifiedName tableName = QualifiedName.of("memory", "default", withQuery.getName().getValue());
            CreateTableAsSelect createTableAsSelectNode = new CreateTableAsSelect(tableName, withQuery.getQuery(), false, ImmutableList.of(), true, Optional.empty(), Optional.empty());
            this.immidiateTableList.add(createTableAsSelectNode);
            DropTable dropTableNode = new DropTable(tableName, false);
            this.dropTableList.add(dropTableNode);
        }
    }

    public String getSql()
    {
        String finalSql = "";
        for (int i = 0; i < this.immidiateTableList.size(); i++) {
            finalSql += printer.print(this.immidiateTableList.get(i));
            finalSql += " ;\n";
        }
        for (int i = 0; i < this.outputQueries.size(); i++) {
            finalSql += printer.print(this.outputQueries.get(i));
            finalSql += " ;\n";
        }
        for (int i = 0; i < this.dropTableList.size(); i++) {
            finalSql += printer.print(this.dropTableList.get(i));
            finalSql += " ;\n";
        }

//        for (String key : this.withTableNameList.keySet()) {
////            System.out.print(key + ": " + this.withTableNameList.get(key).get() + "\n");
//            if (this.withTableNameList.get(key).get() > 0) {
//                finalSql = finalSql.replace(" "+key+" ", " memory.default." + key + " ");
//                finalSql = finalSql.replace(" "+key+"\n", " memory.default." + key + "\n");
//                finalSql = finalSql.replace(" "+key+",", " memory.default." + key + ",");
//            }
//        }
//        System.out.print(finalSql);
        return finalSql;
    }

    private static class CountInt
    {
        private int count;

        public CountInt()
        {
            count = 0;
        }

        public void add()
        {
            count++;
        }

        public int get()
        {
            return count;
        }
    }
}
