/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.prestosql.cli;

import io.prestosql.sql.tree.AliasedRelation;
import io.prestosql.sql.tree.AllColumns;
import io.prestosql.sql.tree.ArithmeticBinaryExpression;
import io.prestosql.sql.tree.BetweenPredicate;
import io.prestosql.sql.tree.Cast;
import io.prestosql.sql.tree.CoalesceExpression;
import io.prestosql.sql.tree.ComparisonExpression;
import io.prestosql.sql.tree.CreateTableAsSelect;
import io.prestosql.sql.tree.DefaultTraversalVisitor;
import io.prestosql.sql.tree.DereferenceExpression;
import io.prestosql.sql.tree.DoubleLiteral;
import io.prestosql.sql.tree.DropTable;
import io.prestosql.sql.tree.FrameBound;
import io.prestosql.sql.tree.FunctionCall;
import io.prestosql.sql.tree.GenericLiteral;
import io.prestosql.sql.tree.GroupBy;
import io.prestosql.sql.tree.Identifier;
import io.prestosql.sql.tree.InListExpression;
import io.prestosql.sql.tree.InPredicate;
import io.prestosql.sql.tree.Intersect;
import io.prestosql.sql.tree.IntervalLiteral;
import io.prestosql.sql.tree.IsNotNullPredicate;
import io.prestosql.sql.tree.IsNullPredicate;
import io.prestosql.sql.tree.Join;
import io.prestosql.sql.tree.JoinOn;
import io.prestosql.sql.tree.Limit;
import io.prestosql.sql.tree.LogicalBinaryExpression;
import io.prestosql.sql.tree.LongLiteral;
import io.prestosql.sql.tree.Node;
import io.prestosql.sql.tree.NullLiteral;
import io.prestosql.sql.tree.OrderBy;
import io.prestosql.sql.tree.Query;
import io.prestosql.sql.tree.QuerySpecification;
import io.prestosql.sql.tree.Rollup;
import io.prestosql.sql.tree.SearchedCaseExpression;
import io.prestosql.sql.tree.Select;
import io.prestosql.sql.tree.SimpleCaseExpression;
import io.prestosql.sql.tree.SimpleGroupBy;
import io.prestosql.sql.tree.SingleColumn;
import io.prestosql.sql.tree.StringLiteral;
import io.prestosql.sql.tree.SubqueryExpression;
import io.prestosql.sql.tree.Table;
import io.prestosql.sql.tree.TableSubquery;
import io.prestosql.sql.tree.Union;
import io.prestosql.sql.tree.WhenClause;
import io.prestosql.sql.tree.Window;
import io.prestosql.sql.tree.WindowFrame;
import io.prestosql.sql.tree.With;
import io.prestosql.sql.tree.WithQuery;

public class TreePrintToSql
{
    public TreePrintToSql() {}

    public String print(Node root)
    {
        DefaultTraversalVisitor<String, Void> printer = new DefaultTraversalVisitor<String, Void>()
        {
            protected boolean needBrackets(Node node)
            {
                if (node instanceof Query
                        || node instanceof ComparisonExpression
                        || node instanceof ArithmeticBinaryExpression
                        || node instanceof SubqueryExpression
                        || node instanceof InListExpression
                        || node instanceof Window
                        || node instanceof LogicalBinaryExpression) {
                    return true;
                }
                return false;
            }

            protected String leftBrackets(Node node)
            {
                if (needBrackets(node)) {
                    return "(";
                }
                return "";
            }

            protected String rightBrackets(Node node)
            {
                if (needBrackets(node)) {
                    return ")";
                }
                return "";
            }

            @Override
            protected String visitNode(Node node, Void context)
            {
                throw new UnsupportedOperationException("not yet implemented: " + node.getClass() + " " + node);
            }

            @Override
            protected String visitQuery(Query node, Void context)
            {
                String queryStr = "";
                if (node.getWith().isPresent()) {
                    queryStr += visitWith(node.getWith().get(), context);
                }
                queryStr += this.process(node.getQueryBody());
//                System.out.print(node.getQueryBody().getClass());
                if (node.getOrderBy().isPresent()) {
                    queryStr += this.process(node.getOrderBy().get());
                    queryStr += "\n";
                }
                if (node.getOffset().isPresent()) {
                    this.process(node.getOffset().get());
                }
                if (node.getLimit().isPresent()) {
                    queryStr += this.process(node.getLimit().get(), context);
                }
                return queryStr;
            }

            @Override
            protected String visitWith(With node, Void context)
            {
                String withStr = "";
                if (node.getQueries().size() > 0) {
                    withStr += "with";
                }

                for (int i = 0; i < node.getQueries().size(); i++) {
                    withStr += visitWithQuery(node.getQueries().get(i), context);
                    if (i != node.getQueries().size() - 1) {
                        withStr += ",";
                    }
                }

                return withStr;
            }

            @Override
            protected String visitWithQuery(WithQuery node, Void context)
            {
                String withQueryStr = "";
                withQueryStr += " ";
                withQueryStr += node.getName().getValue();
                withQueryStr += " ";
                withQueryStr += "as";
                withQueryStr += "\n";
                withQueryStr += "(";
                withQueryStr += visitQuery(node.getQuery(), context);
                withQueryStr += ")";
                return withQueryStr;
            }

            @Override
            protected String visitQuerySpecification(QuerySpecification node, Void context)
            {
                String querySpecifiStr = "";
//                System.out.print("enter sepci\n");
                querySpecifiStr += this.process(node.getSelect());
                if (node.getFrom().isPresent()) {
//                    System.out.print(node.getFrom().get().getClass());
                    querySpecifiStr += "from ";
                    querySpecifiStr += this.process(node.getFrom().get());
                    querySpecifiStr += "\n";
                }
                if (node.getWhere().isPresent()) {
//                    System.out.print(node.getWhere().get().getClass());
//                    System.out.print(node.getWhere().get());
//                    System.out.print("\n");
                    querySpecifiStr += "where ";
                    querySpecifiStr += this.process(node.getWhere().get());
                    querySpecifiStr += "\n";
                }
                if (node.getGroupBy().isPresent()) {
                    querySpecifiStr += "group by ";
                    querySpecifiStr += this.process(node.getGroupBy().get());
                    querySpecifiStr += "\n";
                }
                if (node.getHaving().isPresent()) {
                    querySpecifiStr += "having ";
                    querySpecifiStr += this.process(node.getHaving().get());
                    querySpecifiStr += "\n";
                }
                if (node.getOrderBy().isPresent()) {
                    querySpecifiStr += this.process(node.getOrderBy().get());
                    querySpecifiStr += "\n";
                }
                if (node.getOffset().isPresent()) {
//                    System.out.print(node.getOffset().get().getClass());
                    querySpecifiStr += this.process(node.getOffset().get());
                }
                if (node.getLimit().isPresent()) {
                    querySpecifiStr += this.process(node.getLimit().get());
                }
//                System.out.print("exit sepci\n");
                return querySpecifiStr;
            }

            @Override
            protected String visitSelect(Select node, Void context)
            {
//                System.out.print("enter select\n");
                String selectStr = "select ";
                if (node.isDistinct()) {
                    selectStr += " distinct ";
                }
                for (int i = 0; i < node.getSelectItems().size(); i++) {
                    if (i > 0) {
                        selectStr += ",\n";
                    }
                    selectStr += this.process(node.getSelectItems().get(i));

//                    System.out.print(node.getSelectItems().get(i).getClass());
//                    System.out.print("\n");
                }
//                System.out.print("exit select\n");
                selectStr += "\n";
                return selectStr;
            }

            @Override
            protected String visitIdentifier(Identifier node, Void context)
            {
                String identifierStr = "";
                identifierStr += node.getValue();
                return identifierStr;
            }

            @Override
            protected String visitLongLiteral(LongLiteral node, Void context)
            {
                String longLiteralStr = "";
                longLiteralStr += String.valueOf(node.getValue());
                return longLiteralStr;
            }

            @Override
            protected String visitStringLiteral(StringLiteral node, Void context)
            {
                String stringLiteralStr = "";
                stringLiteralStr += "'";
                stringLiteralStr += node.getValue();
                stringLiteralStr += "'";
                return stringLiteralStr;
            }

            @Override
            protected String visitLimit(Limit node, Void context)
            {
                String limitStr = "";
                limitStr += "limit ";
                limitStr += node.getLimit();
//                System.out.print(node.getLimit());
                limitStr += "\n";
                return limitStr;
            }

            @Override
            protected String visitSingleColumn(SingleColumn node, Void context)
            {
                String singleColumnStr = "";
                singleColumnStr += this.process(node.getExpression());
                singleColumnStr += " ";
                if (node.getAlias().isPresent()) {
                    singleColumnStr += node.getAlias().get();
//                    System.out.print("alias: ");
//                    System.out.print(node.getAlias().get());
//                    System.out.print("\n");
                }
//                System.out.print("expersion: ");
//                System.out.print(node.getExpression().getClass()+" ");
//                System.out.print(node.getExpression());
//                System.out.print("\n");
                return singleColumnStr;
            }

            @Override
            protected String visitFunctionCall(FunctionCall node, Void context)
            {
                String functionCallStr = "";
//                System.out.print(node.toString() + "\n");
//                System.out.print(node.getName() + "\n");
                functionCallStr += node.getName();
                functionCallStr += "(";
                if (node.isDistinct()) {
                    functionCallStr += "distinct ";
                }
                if (node.getArguments().isEmpty() && "count".equalsIgnoreCase(node.getName().getSuffix())) {
                    functionCallStr += "*";
                }
                for (int i = 0; i < node.getArguments().size(); i++) {
                    if (i > 0) {
                        functionCallStr += ",";
                    }
//                    System.out.print(node.getArguments().get(i).getClass());
//                    System.out.print("\n");
                    functionCallStr += this.process(node.getArguments().get(i));
                }
                functionCallStr += ")";
                if (node.getWindow().isPresent()) {
//                    System.out.print(node.getWindow().get() + "\n");
                    functionCallStr += " over ";
                    functionCallStr += "(";
                    functionCallStr += this.process(node.getWindow().get());
                    functionCallStr += ")";
                }
                if (node.getFilter().isPresent()) {
//                    System.out.print(node.getFilter().get() + "\n");
                    throw new UnsupportedOperationException("FunctionCall not yet implemented: " + node.getFilter().get());
                }
                if (node.getOrderBy().isPresent()) {
//                    System.out.print(node.getOrderBy().get() + "\n");
                    throw new UnsupportedOperationException("FunctionCall not yet implemented: " + node.getOrderBy().get());
                }
                return functionCallStr;
            }

            @Override
            protected String visitArithmeticBinary(ArithmeticBinaryExpression node, Void context)
            {
                String arithmeticBinaryStr = "";

                arithmeticBinaryStr += leftBrackets(node.getLeft());
                arithmeticBinaryStr += this.process(node.getLeft());
                arithmeticBinaryStr += rightBrackets(node.getLeft());

                arithmeticBinaryStr += node.getOperator().getValue();

                arithmeticBinaryStr += leftBrackets(node.getRight());
                arithmeticBinaryStr += this.process(node.getRight());
                arithmeticBinaryStr += rightBrackets(node.getRight());

                return arithmeticBinaryStr;
            }

            @Override
            protected String visitOrderBy(OrderBy node, Void context)
            {
                String orderByStr = "";
                orderByStr += "order by ";
                for (int i = 0; i < node.getSortItems().size(); i++) {
                    if (i > 0) {
                        orderByStr += ",";
                    }
                    orderByStr += this.process(node.getSortItems().get(i));
                }
                return orderByStr;
            }

            @Override
            protected String visitJoin(Join node, Void context)
            {
                String joinStr = "";
//                System.out.print(node.getLeft().getClass());
                joinStr += this.process(node.getLeft());
                switch (node.getType()) {
                    case IMPLICIT:
                        joinStr += ", ";
                        break;
                    case LEFT:
                        joinStr += " left outer join ";
                        break;
                    case FULL:
                        joinStr += " full outer join ";
                        break;
                    case INNER:
                        joinStr += " join ";
                        break;
                    default:
                        throw new UnsupportedOperationException("join type not yet implemented: " + node.getType());
                }
                joinStr += this.process(node.getRight());
                if (node.getCriteria().isPresent()) {
//                    System.out.print(node.getCriteria().get());
                    if (node.getCriteria().get() instanceof JoinOn) {
                        joinStr += " on ";
                        joinStr += leftBrackets(((JoinOn) node.getCriteria().get()).getExpression());
                        joinStr += ((JoinOn) node.getCriteria().get()).getExpression();
                        joinStr += rightBrackets(((JoinOn) node.getCriteria().get()).getExpression());
                    }
                }
                return joinStr;
            }

            @Override
            protected String visitTable(Table node, Void context)
            {
                String tableStr = "";
                tableStr += node.getName();
//                System.out.print(node);
                return tableStr;
            }

            @Override
            protected String visitTableSubquery(TableSubquery node, Void context)
            {
                String tableSubQueryStr = "";
//                System.out.print(node);
                tableSubQueryStr += "(";
                tableSubQueryStr += this.process(node.getQuery());
                tableSubQueryStr += ")";
                return tableSubQueryStr;
            }

            @Override
            protected String visitIntersect(Intersect node, Void context)
            {
                String intersectStr = "";
                for (int i = 0; i < node.getRelations().size(); i++) {
                    if (i > 0) {
                        intersectStr += "intersect\n";
                    }
                    intersectStr += this.process(node.getRelations().get(i));
//                    System.out.print(node.getRelations().get(i).getClass());
//                    System.out.print("\n");
                }
                return intersectStr;
            }

            @Override
            protected String visitDereferenceExpression(DereferenceExpression node, Void context)
            {
                String dereferenceExpressionStr = "";
                dereferenceExpressionStr += this.process(node.getBase());
                dereferenceExpressionStr += ".";
                dereferenceExpressionStr += node.getField();
                return dereferenceExpressionStr;
            }

            @Override
            protected String visitLogicalBinaryExpression(LogicalBinaryExpression node, Void context)
            {
                String logicalBinaryExpressionStr = "";
//                System.out.print(node.getLeft().getClass());
//                System.out.print("\n");
//                System.out.print(node.getRight().getClass());
//                System.out.print("\n");
                logicalBinaryExpressionStr += leftBrackets(node.getLeft());
                logicalBinaryExpressionStr += this.process(node.getLeft());
                logicalBinaryExpressionStr += rightBrackets(node.getLeft());

                logicalBinaryExpressionStr += "\n";
                logicalBinaryExpressionStr += node.getOperator().toString();
                logicalBinaryExpressionStr += " ";

                logicalBinaryExpressionStr += leftBrackets(node.getRight());
                logicalBinaryExpressionStr += this.process(node.getRight());
                logicalBinaryExpressionStr += rightBrackets(node.getRight());
                return logicalBinaryExpressionStr;
            }

            @Override
            protected String visitComparisonExpression(ComparisonExpression node, Void context)
            {
                String comparisonExpressionStr = "";

//                System.out.print(node.getLeft().getClass() + " " + node.getLeft() +"\n");
//                System.out.print(node.getRight().getClass() + " " + node.getRight() +"\n");

                comparisonExpressionStr += leftBrackets(node.getLeft());
                comparisonExpressionStr += this.process(node.getLeft());
                comparisonExpressionStr += rightBrackets(node.getLeft());

                comparisonExpressionStr += " ";
                comparisonExpressionStr += node.getOperator().getValue();
                comparisonExpressionStr += " ";

                comparisonExpressionStr += leftBrackets(node.getRight());
                comparisonExpressionStr += this.process(node.getRight());
                comparisonExpressionStr += rightBrackets(node.getRight());

                return comparisonExpressionStr;
            }

            @Override
            protected String visitBetweenPredicate(BetweenPredicate node, Void context)
            {
                String betweenPredicateStr = "";
                betweenPredicateStr += this.process(node.getValue());
                betweenPredicateStr += " between ";
//                System.out.print(node.getMin().getClass()+"\n");

                betweenPredicateStr += leftBrackets(node.getMin());
                betweenPredicateStr += this.process(node.getMin());
                betweenPredicateStr += rightBrackets(node.getMin());

                betweenPredicateStr += " and ";
//                System.out.print(node.getMax().getClass()+"\n");

                betweenPredicateStr += leftBrackets(node.getMax());
                betweenPredicateStr += this.process(node.getMax());
                betweenPredicateStr += rightBrackets(node.getMax());

                return betweenPredicateStr;
            }

            @Override
            protected String visitUnion(Union node, Void context)
            {
                String unionStr = "";
//                System.out.print(node.isDistinct());
//                System.out.print("\n");
                for (int i = 0; i < node.getRelations().size(); i++) {
                    if (i > 0) {
                        unionStr += "union";
                        if (!node.isDistinct()) {
                            unionStr += " all";
                        }
                        unionStr += "\n";
                    }
                    unionStr += this.process(node.getRelations().get(i));
//                    System.out.print(node.getRelations().get(i).getClass());
//                    System.out.print("\n");
                }
                return unionStr;
            }

            @Override
            protected String visitInPredicate(InPredicate node, Void context)
            {
                String inPredicateStr = "";
                inPredicateStr += this.process(node.getValue());
                inPredicateStr += " in ";

                inPredicateStr += leftBrackets(node.getValueList());
//                System.out.print(node.getValueList().getClass() + "\n");
                inPredicateStr += this.process(node.getValueList());
                inPredicateStr += rightBrackets(node.getValueList());

                return inPredicateStr;
            }

            @Override
            protected String visitGroupBy(GroupBy node, Void context)
            {
                String groupbyStr = "";
                for (int i = 0; i < node.getGroupingElements().size(); i++) {
                    if (i > 0) {
                        groupbyStr += ", ";
                    }
                    groupbyStr += this.process(node.getGroupingElements().get(i));
//                    System.out.print(node.getGroupingElements().get(i).getClass());
//                    System.out.print("\n");
                }
                return groupbyStr;
            }

            @Override
            protected String visitSimpleGroupBy(SimpleGroupBy node, Void context)
            {
                String simpleGroupByStr = "";
                for (int i = 0; i < node.getExpressions().size(); i++) {
                    if (i > 0) {
                        simpleGroupByStr += ", ";
                    }
                    simpleGroupByStr += this.process(node.getExpressions().get(i));
                }
                return simpleGroupByStr;
            }

            @Override
            protected String visitRollup(Rollup node, Void context)
            {
                String rollupStr = "";
                rollupStr += "rollup (";
                for (int i = 0; i < node.getExpressions().size(); i++) {
                    if (i > 0) {
                        rollupStr += ", ";
                    }
                    rollupStr += this.process(node.getExpressions().get(i));
                }
                rollupStr += ")";
                return rollupStr;
            }

            @Override
            protected String visitCreateTableAsSelect(CreateTableAsSelect node, Void context)
            {
                String createTableAsSelectStr = "";
                createTableAsSelectStr += "CREATE TABLE ";
                createTableAsSelectStr += node.getName().toString();
                createTableAsSelectStr += " as ";
                createTableAsSelectStr += "(";
                createTableAsSelectStr += this.process(node.getQuery());
                createTableAsSelectStr += ")";
                return createTableAsSelectStr;
            }

            @Override
            protected String visitDropTable(DropTable node, Void context)
            {
                String dropTableStr = "DROP TABLE ";
                dropTableStr += node.getTableName().toString();
                return dropTableStr;
            }

            @Override
            protected String visitAliasedRelation(AliasedRelation node, Void context)
            {
                String aliasedRelationStr = "";
                aliasedRelationStr += this.process(node.getRelation());
                aliasedRelationStr += " ";
                aliasedRelationStr += node.getAlias().getValue();
                return aliasedRelationStr;
            }

            @Override
            protected String visitNullLiteral(NullLiteral node, Void context)
            {
                String nullLiteralStr = "";
                nullLiteralStr += "null";
//                System.out.print(node + "\n");
                return nullLiteralStr;
            }

            @Override
            protected String visitSearchedCaseExpression(SearchedCaseExpression node, Void context)
            {
                String searchedCaseExpressionStr = "";
                searchedCaseExpressionStr += "case ";
                for (int i = 0; i < node.getWhenClauses().size(); i++) {
                    if (i > 0) {
                        searchedCaseExpressionStr += " ";
                    }
                    searchedCaseExpressionStr += this.process(node.getWhenClauses().get(i));
//                    System.out.print(node.getWhenClauses().get(i));
//                    System.out.print("\n");
                }
                if (node.getDefaultValue().isPresent()) {
                    searchedCaseExpressionStr += " else ";
                    searchedCaseExpressionStr += this.process(node.getDefaultValue().get());
                }

                searchedCaseExpressionStr += " end";
//                System.out.print(node.getDefaultValue().get());
//                System.out.print("\n");

                return searchedCaseExpressionStr;
            }

            @Override
            protected String visitWhenClause(WhenClause node, Void context)
            {
                String whenClauseStr = "";
                whenClauseStr += "when ";
                whenClauseStr += this.process(node.getOperand());
                whenClauseStr += " then ";
                whenClauseStr += this.process(node.getResult());
//                System.out.print(node.getOperand().getClass());
//                System.out.print("\n");
//                System.out.print(node.getResult().getClass());
//                System.out.print("\n");
                return whenClauseStr;
            }

            @Override
            protected String visitCast(Cast node, Void context)
            {
                String castStr = "";
                castStr += "cast(";
                castStr += this.process(node.getExpression());
                castStr += " as ";
                castStr += node.getType();
                castStr += ")";
                return castStr;
            }

            @Override
            protected String visitIntervalLiteral(IntervalLiteral node, Void context)
            {
                String intervalLiteralStr = "";
                intervalLiteralStr += "INTERVAL ";
                intervalLiteralStr += "\'";
                intervalLiteralStr += node.getValue();
                intervalLiteralStr += "\' ";
                intervalLiteralStr += node.getStartField();
//                System.out.print(node.getStartField()+"\n");
//                System.out.print(node.getEndField()+"\n");
//                System.out.print(node.getSign()+"\n");
//                System.out.print(node.getValue()+"\n");
                return intervalLiteralStr;
            }

            @Override
            protected String visitDoubleLiteral(DoubleLiteral node, Void context)
            {
                String doubleLiteralStr = "";
                doubleLiteralStr += node.getValue();
                return doubleLiteralStr;
            }

            @Override
            protected String visitAllColumns(AllColumns node, Void context)
            {
                String allColumnsStr = "";
                allColumnsStr += "*";
                return allColumnsStr;
            }

            @Override
            protected String visitInListExpression(InListExpression node, Void context)
            {
                String inListExpressionStr = "";
                for (int i = 0; i < node.getValues().size(); i++) {
                    if (i > 0) {
                        inListExpressionStr += ", ";
                    }
                    inListExpressionStr += this.process(node.getValues().get(i));
//                    System.out.print(node.getWhenClauses().get(i));
//                    System.out.print("\n");
                }
                return inListExpressionStr;
            }

            @Override
            protected String visitSimpleCaseExpression(SimpleCaseExpression node, Void context)
            {
                String simpleCaseExpressionStr = "";
                simpleCaseExpressionStr += "case ";
//                System.out.print(node.getOperand().getClass());
                simpleCaseExpressionStr += this.process(node.getOperand());
                simpleCaseExpressionStr += " ";
                for (int i = 0; i < node.getWhenClauses().size(); i++) {
                    if (i > 0) {
                        simpleCaseExpressionStr += " ";
                    }
                    simpleCaseExpressionStr += this.process(node.getWhenClauses().get(i));
//                    System.out.print(node.getWhenClauses().get(i));
//                    System.out.print("\n");
                }
                if (node.getDefaultValue().isPresent()) {
                    simpleCaseExpressionStr += " else ";
                    simpleCaseExpressionStr += this.process(node.getDefaultValue().get());
                }

                simpleCaseExpressionStr += " end";
                return simpleCaseExpressionStr;
            }

            @Override
            protected String visitGenericLiteral(GenericLiteral node, Void context)
            {
                String genericLiteralStr = "";
                genericLiteralStr += node.getType();
                genericLiteralStr += "\'";
                genericLiteralStr += node.getValue();
                genericLiteralStr += "\'";
                return genericLiteralStr;
            }

            @Override
            protected String visitCoalesceExpression(CoalesceExpression node, Void context)
            {
                String coalesceExpressionStr = "";
                coalesceExpressionStr += "coalesce(";
                for (int i = 0; i < node.getOperands().size(); i++) {
                    if (i > 0) {
                        coalesceExpressionStr += ", ";
                    }
                    coalesceExpressionStr += this.process(node.getOperands().get(i));
//                    System.out.print(node.getWhenClauses().get(i));
//                    System.out.print("\n");
                }
                coalesceExpressionStr += ")";
                return coalesceExpressionStr;
            }

            @Override
            public String visitWindow(Window node, Void context)
            {
                String windowStr = "";
//                System.out.print(node.getFrame().get()+"\n");
//                System.out.print(node.getOrderBy()+"\n");
//                System.out.print(node.getPartitionBy()+"\n");
                windowStr += "partition by ";
                for (int i = 0; i < node.getPartitionBy().size(); i++) {
                    if (i > 0) {
                        windowStr += ", ";
                    }
                    windowStr += this.process(node.getPartitionBy().get(i));
//                    System.out.print(node.getPartitionBy().get(i));
//                    System.out.print("\n");
                }
                windowStr += "\n";
                if (node.getOrderBy().isPresent()) {
                    windowStr += this.process(node.getOrderBy().get());
                }
                if (node.getFrame().isPresent()) {
                    windowStr += this.process(node.getFrame().get());
                }

                return windowStr;
            }

            @Override
            public String visitWindowFrame(WindowFrame node, Void context)
            {
                String windowFrame = "";
                windowFrame += " ";
                windowFrame += node.getType().toString();
                windowFrame += " between ";
                windowFrame += this.process(node.getStart());
                if (node.getEnd().isPresent()) {
                    windowFrame += " and ";
                    windowFrame += this.process(node.getEnd().get());
                }
                else {
                    throw new UnsupportedOperationException("windowFrame not yet implemented: " + node);
                }
                return windowFrame;
            }

            @Override
            public String visitFrameBound(FrameBound node, Void context)
            {
                String frameBoundStr = "";
                if (node.getValue().isPresent()) {
                    throw new UnsupportedOperationException("FrameBound not yet implemented: " + node);
                }
                switch (node.getType()) {
                    case UNBOUNDED_PRECEDING:
                        frameBoundStr += "unbounded preceding";
                        break;
                    case CURRENT_ROW:
                        frameBoundStr += "current row";
                        break;
                    default:
                        throw new UnsupportedOperationException("FrameBound type not yet implemented: " + node.getType());
                }

                return frameBoundStr;
            }

            @Override
            protected String visitIsNotNullPredicate(IsNotNullPredicate node, Void context)
            {
                String isNotNullPredicateStr = "";
                isNotNullPredicateStr += this.process(node.getValue());
                isNotNullPredicateStr += " is not NULL";
                return isNotNullPredicateStr;
            }

            @Override
            protected String visitIsNullPredicate(IsNullPredicate node, Void context)
            {
                String isNullPredicateStr = "";
                isNullPredicateStr += this.process(node.getValue());
                isNullPredicateStr += " is null";
                return isNullPredicateStr;
            }
        };

        return printer.process(root);
    }
}
